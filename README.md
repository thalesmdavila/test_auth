**Generate token from body data**

```
curl -X POST -H "Content-Type: application/json" \    -d '{"email": "gestorx@test.com", "name": "Gestor x", "service_x_user": "test_service_x_user", "service_x_pwd": "test_service_x_pwd"}' \
    http://localhost:3000/generate
```

**Verify and get data**

```
curl -H "token: <verify_token_from_previous_request>" http://localhost:3000/verify
```
