// server.js
// importing express 
const express = require('express');
// creating express instance
const app = express();
 
// importing body-parser, which will handle the user input bodies
const bodyParser = require('body-parser');
 
// importing jsonwebtoken module, this module will create and decode the JWT tokens.
const jsonWebToken = require('jsonwebtoken');
 
app.use(bodyParser.json()); // only parses json data
app.use(bodyParser.urlencoded({ // handles the urlencoded bodies
    extended: true
}));
 
const myJWTSecretKey = 'my-secret-key'; // set secret key, which we will use for creating and decoding JWT tokens, keep it safe.
 
// POST - http://localhost:3000/generate
app.post('/generate', (req, res) => {
    const user = {
        email: req.body.email,
        name: req.body.name,
        service_x_user: req.body.service_x_user,
        service_x_pwd: req.body.service_x_pwd,
    };

    // before generating it shoud verify if the manager is eligible to have api access

    // sign with default (HMAC SHA256) 
    const token = jsonWebToken.sign(user, myJWTSecretKey);
    res.json({
        token: token
    });
});
 
// GET - http://localhost:3000/verify
app.get('/verify', (req, res) => {
    // verify if token is ok and return the user data
    try {
        const tokenDecodedData = jsonWebToken.verify(req.header('token'), myJWTSecretKey);
        return res.json({
            error: false,
            data: tokenDecodedData
        });
    } catch (error) {
        res.json({
            error: true,
            data: error
        });
    }
})
 
app.listen(3000, () => {
    console.log(`Server is running at: 3000`);
});